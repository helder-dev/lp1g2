/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp1g3;
import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class LP1G3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double a,b,c;

        Scanner scan = new Scanner(System.in);


        System.out.println("Qual o valor do comprimento? ");
        a = scan.nextDouble();
        if ( a <= 0){
            do{
                System.out.println("Valor introduzido inválido. O comprimento têm que ser maior que zero. Introduza novamente");
                a = scan.nextDouble();
            } while ( a <= 0);
        }

        System.out.println("Qual o valor da largura? ");
        b = scan.nextDouble();
        if (b <= 0){
            do{
                System.out.println("Valor introdúzido inválido. O comprimento têm que ser maior que zero. Introduza novamente");
                b = scan.nextDouble();
            } while ( b <= 0 );
        }

        System.out.println("Qual o valor da altura?");
        c = scan.nextDouble();
        if (c <= 0){
            do{
                System.out.println("Valor introdúzido inválido. O comprimento têm que ser maior que zero. Introduza novamente");
                c = scan.nextDouble();
            } while ( c <= 0 );
        }



        System.out.println("A área total das bases é : " + calcularAreaBases(a,b));
        System.out.println("A área lateral é : " + calcularAreaLateral(a,c));
        System.out.println("A área é : " + calcularAreaTotal(a,b));
        System.out.println("O volume é : " + calcularVolume(a,b,c)); 
    }
    
    /**
     * Função que devolve a área das 2 bases de um paralelipipedo
     * 
     * @param a 
     * @param b 
     * @return 
     */
    static public double calcularAreaBases(double a, double b){
        return (a * b) * 2;
    }
    
    /**
     * Função que devolve a área das 4 laterais de um paralelipipedo
     * @param a
     * @param b
     * @return 
     */
    static public double calcularAreaLateral(double a, double b){
        return (a * b) * 4;
    }
    
    /**
     * Função que devolve a área total de um paralelipipedo
     * @param a
     * @param b
     * @return 
     */
    static public double calcularAreaTotal(double a, double b){
        return calcularAreaLateral(a,b) + calcularAreaBases(a,b);
    }
    
    /**
     * Função que devolve o volume de um paralelipipedo
     * @param a
     * @param b
     * @param c
     * @return 
     */
    static public double calcularVolume(double a, double b, double c){
        return a*b*c;
    }
    
    
}
