/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp1f4g1;
import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class LP1F4G1 {
    public static int GetInt(String Message){
        System.out.print(Message);
        return  (new Scanner(System.in)).nextInt();}
        
        public static double GetDouble(String Message){
        System.out.print(Message);
        return  (new Scanner(System.in)).nextDouble();}

        public static void PLN(String escrever){ System.out.println(escrever);}
    
    public static Scanner scan = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int c = GetInt("Digite o número de colunas: ");
        int l = GetInt("Digite o número de linhas: ");
        
        int [][] A = new int[l][c];
        
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                A[i][j] = GetInt("Digite o número para a posição [" + i + "][" + j + "] da matriz: ");
            }
        }
        
        verificarOcorrencias(A, c, l);
        
    }
 
    public static void verificarOcorrencias( int matriz[][] , int c , int l ){
        int idxI = 0, idxJ = 0;
        int[][] matrizOutput = new int[2][(c * l) - 1];
       
        for (int i = 0 ; i<l ; i++){
            for (int j = 0 ; j<c ; j++){
                int pos = contemElemento(matriz[i][j], matrizOutput, ((c * l) - 1));
                if (pos >= 0) {
                    matrizOutput[1][pos] = matrizOutput[1][pos] + 1;//EXISTE
                } else {
                    for (int x = 0; x < ((c * l) - 1); x++) {
                        if (matrizOutput[0][x] == 0) {
                            matrizOutput[0][x] = matriz[i][j];
                            matrizOutput[1][x] = 1;
                            break;
                        }
                    }
                }
                
            }
        }
        
        ordenarMatriz(matrizOutput, ((c * l) - 1));
    }
    
    public static int contemElemento(int value, int matrizOutput[][], int c) {
        int retPosicao = -1;
        for (int i = 0; i < c; i++) {
            if (matrizOutput[0][i] == value) {
                retPosicao = i;
                break;
            }
        }
        
        return retPosicao;
    }
    
    public static void matrizOutput(int matriz[][], int c) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("");
        }
        
    }
    
    public static void ordenarMatriz(int matriz[][], int c){
        for (int i = (c-1); i > 0 ; i--){
            if (matriz[1][i] > matriz[1][i -1]){
                int temp = matriz [1][i];
                int temp2 = matriz[0][i];

                matriz[1][i] = matriz[1][i-1];
                matriz[0][i] = matriz[0][i-1];

                matriz[1][i-1] = temp;
                matriz[0][i-1] = temp2;
            }
        }
        matrizOutput(matriz, c);
    } 
    
    
}
