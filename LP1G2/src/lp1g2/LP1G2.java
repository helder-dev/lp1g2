/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp1g2;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class LP1G2 {
    public static int GetInt(String Message){
        System.out.print(Message);
        return (new Scanner(System.in)).nextInt();
    }
        
    public static double GetDouble(String Message){
        System.out.print(Message);
        return (new Scanner(System.in)).nextDouble();
    }
        
    public static String GetString(String Message){
        System.out.print(Message);
        return (new Scanner(System.in)).next();
    }

    public static void PLN(String escrever){ System.out.println(escrever); }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String solido;
        double raio=0, altura=0;
        
        do {
            solido = GetString("Digite o nome do sólido (cilindro, cone ou esfera. Digite fim para terminar o programa: ");
            
            if (Objects.equals(solido, "cilindro") || Objects.equals(solido, "cone")) {
                raio = GetDouble("Digite o raio: ");
                altura = GetDouble("Digite a sua altura: ");  
            } else if(Objects.equals(solido, "esfera")) {
                double raioesfera = GetDouble("Digite o raio da esfera: ");
                PLN("Aréa da esfera é: " + calcularVolumeEsfera(raioesfera));
            }
            
            switch (solido) {
                case "cilindro": 
                    PLN("Aréa do cilindro é: " + calcularVolumeCilindro(raio,altura));
                    break;
                case "cone": 
                    PLN("Aréa do cone é: " + calcularVolumeCone(raio,altura));
                    break;
                
                case "FIM": PLN("Fim do programa.");
                    break;
                    
                default: PLN("Digite um sólido válido");
                    break;
            }
            
        } while (!(Objects.equals(solido, "FIM")));
    }
    
    /**
     * Calcular volume do cone
     * 
     * @param raio
     * @param altura
     * @return 
     */
    static public double calcularVolumeCone(double raio, double altura) {
        return (Math.PI * Math.pow(raio, 2) * altura) / 3;
    }
    
    /**
     * Calcular volume da esfera
     * @param raio
     * @return 
     */
    static public double calcularVolumeEsfera( double raio) {
        return (4 * Math.PI * Math.pow(raio,3)) / 3;
    }

    /**
     * Calcular volume do cilindro
     * @param raio
     * @param altura
     * @return 
     */
    static public double calcularVolumeCilindro(double raio, double altura){
        return (Math.PI * Math.pow(raio, 2 * altura));
    }
}
